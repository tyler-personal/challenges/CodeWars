module First where
import Data.List

data Item = Value Int | Range Int Int

instance Show Item where
  show (Value x) = show x
  show (Range x y) = show x ++ "-" ++ show y

solution :: [Int] -> String
solution = intercalate "," . map show . consolidate . map Value

consolidate :: [Item] -> [Item]
consolidate (Value x : Value y : Value z : xs)
  | [x,y,z] == [x..x+2] = consolidate (Range x z : xs)
  | otherwise = Value x : consolidate (Value y : Value z : xs)
consolidate (Range x y : Value z : xs)
  | y + 1 == z = consolidate (Range x z : xs)
  | otherwise = Range x y : consolidate (Value z : xs)
consolidate xs = xs
