{-# LANGUAGE NoMonomorphismRestriction #-}
import Control.Applicative
import Control.Monad
import Data.List
import Data.Char

f :: (Int -> Int) -> (Int -> Int)
f g = g

isIsogram' str = length str == (length . nub) str

isIsogram = liftA2 (==) length (length . nub . map toLower)

isIsogram2 = ap (==) nub . map toLower

isIsogram3 = (==) <*> nub
isIsogram3' str = str == nub str

main = undefined
