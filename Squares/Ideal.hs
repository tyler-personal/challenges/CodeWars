-- This version doesn't use `read` or `show` as str -> int conversions are inherently unsafe
module Ideal where
import Data.List
import Control.Monad
import Control.Applicative
import Data.Functor
import Data.Tuple

squareDigits :: Int -> Int
squareDigits = fromDigits . concatMap (toDigits . (^ 2)) . toDigits

toDigits :: Int -> [Int]
toDigits = reverse . unfoldr (\x -> guard (x /= 0) $> modDiv x 10)

fromDigits :: [Int] -> Int
fromDigits = foldl ((+) . (* 10)) 0

modDiv :: Int -> Int -> (Int, Int)
modDiv = swap .: divMod

(.:) = (.) . (.)
--toDigits = reverse . unfoldr (($>) <$> guard . (0 /=) <*> (`modDiv` 10))
