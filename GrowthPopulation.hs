module Main where

f startPop percent amount endPop = snd $ until (\(pop,_) -> pop >= endPop) (\(pop,i) -> (pop + pop * percent + amount, i+1)) (startPop, 0)

g :: Int -> Double -> Int -> Int -> Int
g startPop' percent amount' endPop' = (+n) $ length . takeWhile (< endPop) $ iterate (\p -> p + p * (percent / 100) + amount) startPop
  where
    [startPop, amount, endPop] = map fromIntegral [startPop', amount', endPop']
    n = if fromIntegral endPop' `mod` 100 == 0 then 0 else 1


main = print 4
