module Rot13 where

import Control.Applicative
import Data.List
import Data.Maybe

rot13 :: String -> String
rot13 [] = []
rot13 (x:xs) = fromMaybe x (get ['a'..'z']<|> get ['A'..'Z']) : rot13 xs
  where get ys = (cycle ys !!) . (+ 13) <$> (x `elemIndex` ys)
